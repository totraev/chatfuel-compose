# Chatfuel app
=========================
## Clone

```git clone --recurse-submodules ...```

## Start with Docker

Docker & docker-compose should be installed

```docker-compose up -d```

```docker-compose run backend yarn seed```

Open http://localhost:3000/

## Manual start

*MomgoDB* should be installed. Set mongo url (defailt: `mongodb://localhost:27017/users`)

```export MONGO_URL=mongodb://localhost:27017/users```

From *backend* directory

```yarn install```

```yarn serve```

```yarn seed```

From *frontend* directory

```yarn install```

```yarn serve```